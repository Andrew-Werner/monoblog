+++
date = 2020-04-29T04:00:00Z
description = ""
tags = ["personal", "studies"]
title = "studies i like"

+++
<iframe src="https://drive.google.com/file/d/1TbIN5KD1ouGtrOLeIqyd_apULh0n1v3G/preview" class="lazyload qcontrol" height="480"></iframe>

[**_Δ9-Tetrahydrocannabinol (THC) impairs visual working memory performance: a randomized crossover trial_**](https://www.nature.com/articles/s41386-020-0690-3?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+npp%2Frss%2Fcurrent+%28Neuropsychopharmacology+-+Issue%29)

 <iframe src="https://drive.google.com/file/d/1ttTsYX0bGmYW96xDG_Q2kO8o1qGCSfMI/preview" class="lazyload qcontrol" height="480"></iframe>

[**_Psilocybin and LSD have no long-lasting effects in an animal model of alcohol relapse_**](https://www.nature.com/articles/s41386-020-0694-z?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+npp%2Frss%2Fcurrent+%28Neuropsychopharmacology+-+Issue%29)

<iframe src="https://drive.google.com/file/d/1nMqX-UuTUU-B-AXfM0cLjqJnV6FE4a6y/preview" class="lazyload" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1Hht4F3algEcf2MHHRkBfH7T0zQ-hiMhF/preview" class="lazyload" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1oYtT4wykws7RSpcAsmmXCmdZFw6nWHxQ/preview" class="lazyload qcontrol" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1G6Xhht3AsM_nVyPgXJB1XMLuwQlVI7tL/preview" class="lazyload qcontrol" height="480"></iframe>

<div class="feedgrabbr_widget lazyload qcontrol" id="fgid_e721ad443b1232f135170d03b"></div>

<script>if (typeof (fg_widgets) === "undefined") fg_widgets = new Array(); fg_widgets.push("fgid_e721ad443b1232f135170d03b");</script>

<script async src="https://www.feedgrabbr.com/widget/fgwidget.js"></script>