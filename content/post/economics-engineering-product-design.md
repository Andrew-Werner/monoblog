+++
date = 2020-04-27T08:20:00Z
description = ""
tags = ["design"]
title = "product design"

+++
product design ramblings

{{< youtube id="CaiMjwF0a64" >}}

the arp odyssey's control panel is simple yet designed to educate its users. it had to be intuitive. there wasn't really stuff like it before it. these **little things make the product good**. _good_ products get out of the way of the user and allow customers to efficiently learn them. **_better_ products are intuitive from the start**. [google's material design](https://material.io/design/) approached this by transforming interface design to mimic what users were already familiar with - paper

{{< youtube id="iKdHES4fdyQ" >}}

it costs more to buy precisely quality controlled components and materials, though with markup and economies of scale, a profit can be easily achieved - especially with novel features and a modern design. high quality products at lower prices will always sell when marketed effectively, though revenue will suffer if compared to companies which operate to extract as much profit as possible

one avenue to sustainable profits and longevity lies in creating an inescapable ecosystem like apple's blue bubbled imessage or the proprietary software/hardware dependence of universal audio's products while practicing [planned obsolescence](https://en.wikipedia.org/wiki/Planned_obsolescence)

<blockquote class='rg_standalone_container' data-src='//genius.com/annotations/11489100/standalone_embed'><a href='[https://genius.com/11489100/Frank-ocean-chanel/This-a-cult-not-a-clique-on-the-net](https://genius.com/11489100/Frank-ocean-chanel/This-a-cult-not-a-clique-on-the-net "https://genius.com/11489100/Frank-ocean-chanel/This-a-cult-not-a-clique-on-the-net")'>This a cult, not a clique on the net</a><br><a href='[https://genius.com/Frank-ocean-chanel-lyrics](https://genius.com/Frank-ocean-chanel-lyrics "https://genius.com/Frank-ocean-chanel-lyrics")'>― Frank Ocean – Chanel</a></blockquote><script async crossorigin src='//genius.com/annotations/load_standalone_embeds.js'></script>

the cult of imessage and other such practices always hurt customer relations, and furthermore require an autonomous team that can solve problems while working on new releases. in the current product market, updates and firmware releases seem to be used primarily as a tool for appeasement of upset customers as companies develop new iterations to push to market. exchange and upgrade policies seem to be reserved for third party resellers or used in times of increased competition or holiday sales. i'm not quite sure the ideal balance of price between supporting the business and appeasing the customer, though some companies are clearly doing it right in a scalable, mass market way

{{< youtube id="DrMgwd-cnak" >}}

behringer gets accused of merely copying designs from other companies, though i love their products as a customer because they are cheap and sound good for the most part. i don't really care that the components may be cheap and mass produced, i just appreciate that you can get something that sounds _like_ a [minimoog for $300](https://www.behringer.com/Categories/Behringer/Keyboards/Synthesizers-and-Samplers/MODEL-D/p/P0CQJ)