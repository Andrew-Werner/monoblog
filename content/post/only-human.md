+++
date = 2020-05-10T04:00:00Z
description = ""
tags = ["personal"]
title = "only human"

+++
my best friend and most favorite person since preschool killed himself at 19. at least he’s at peace.

a tiny dude with the biggest heart, he was a saint. it will forever depress me seeing a good kid with a good family go like that.

first he went missing, and then his mom called me. nothing made sense. he never ran away, snuck out, or attempted suicide before. i stayed up all night thinking about him coming to the thought in my mother’s arms that I would rather have him kill himself than be abducted.  
A day goes by.

The whole community searches together during the quarantine of corona virus (I’m told to not look - for the best).

His body is found with no signs of struggle or foul play.

I read that shit on Facebook.

I shouted in my house, my face collapsed, I ran outside pulling at my hair. Across the street was a lady walking. I’m doing what must look like some maniac shit yelling incoherent noises of despair and disbelief running towards his house leaving a trail of tears.

My parents whip their car around the corner I’m running towards, my mom jumps out of the moving car and hugs me. “We wanted to get back before you found out! Let’s go home.”

Fuck no. I was running to the parents.

Back in the car, we speed down the street to a mob of people hugging the father. The collective pain is indescribable. After a few seconds he sees me, comes to me, we embrace hard. We scream, sink into each other. I just say thank you.  
“The 3 of us had some great fucking times together.” Tears everywhere. Everyone is grasping empty-handedly at the sky.

His mom was the one holding all the shit together. She was in the house on the phone comforting his friends.

She comes out, we hug. Harder. She cries into me - this is the woman who took care of me when I used to escape my household for theirs constantly for over a decade and a half. “He was just so smart.” He was at northeastern for computer science and he knew big things about the world better than you do - he could also sing **good**. “so quiet, can’t ask for help” erhhh! “last year around this time I was in the hospital for a suicide attempt” I tell her. “It’s silent. I’m sorry” I’m sure there were more tears and gasps for breaths, but I said something close along those lines - i did good.

“You are loved.”

200 users, some of them whole families, participated in his virtual candle lighting during quarantine.

this shit is fucking hard. this world can be too cold if you don’t surround yourself with love. don’t be quiet. ask for help. be vulnerable. it is only human.

here is what i sent his dad, i hope it helps open your eyes to the pain that is felt after suicide:

![](/uploads/81D7B73E-D638-4240-BDEB-378C05669737.jpeg)  
![](/uploads/DF38E481-C239-49A8-A993-3AFBA24E3AC3.jpeg)  
the night he passed he added me to a discord server with his college friends and had organized a terraria server for the weekend. they seemed wholesome and good like him :)

a couple weeks ago we were hiking reconnecting about college. he seemed disappointed with the system, though not crushed.

about a week and a half ago he was at my house and we played magicka and laughed our asses off like we were kids. he gave me his philip glass solo piano book. i thought it was just a kind gesture, but i can’t help but run my mind as to whether or not he had been planning his death.

some things we can never know.

bye thank you your welcome, friend

(him and i used to abbreviate all the manners to each other’s parents as bye thank you your welcome delivered all at once)