+++
date = 2020-04-15T08:20:00Z
description = "3 signal chains for guitars pulled from kenny beat's twitch live stream"
tags = ["signal-chain", "music"]
title = "kenny beats guitar signal chain"

+++
_for_ **educational** _purposes_

![](/uploads/kenny beats guitar.jpg "kenny beats guitar signal chain")

{{< youtube id="i86bG2OsOSQ" >}}