+++
date = 2020-04-21T04:00:00Z
description = ""
title = "modes {wip}"
tags = ["music"]

+++
demo of modes

<hr>

_🚧 under construction 👷‍♂️_ <hr> <div class="keyboard-options lazyload"> Sound <select id="sound"> <option value="0" selected="">mellow keys</option> <option value="1">dream</option> <option value="2">pluck</option> <option value="3">loch ness</option> </select> <p></p> Scale <select id="scale"> <option value="0" selected="">not ready</option> <option value="1">check</option> <option value="2">back</option> <option value="3">later</option> </select> <div id="keyboard" class="keyboard-holder" style="width: 100%;"></div> <div class="keyboard-options"><input type="button" id="-_OCTAVE" value="-"> C<span id="OCTAVE_LOWER">3</span> - B<span id="OCTAVE_UPPER">3</span> <input type="button" id="+_OCTAVE" value="+"><br><i>+|- with l/r arrow keys</i>
</div>

<script type="text/javascript">

var a = new AudioSynthView();
a.draw();

</script>

</div>