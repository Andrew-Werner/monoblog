+++
date = 2020-04-20T04:00:00Z
description = ""
tags = ["music"]
title = "harmonics"

+++
adding harmonic content is a process of multiple takes, practicing good gain staging, switching up mics, tubes, preamps and/or instruments between takes, layering multiple tracks, stretching the mix out, mid-side processing it, making sure everything sits well, saving compression and other processing for the waveform after, and just acoustically treating the room to taste for the most part