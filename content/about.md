+++
blocks = []
body = ""
date = ""
gallery = ["/uploads/IMG_2377.jpg", "/uploads/RNI-Films-IMG-E0C34F47-200D-4E2F-B368-F7EA664ECB30.JPG", "/uploads/IMG_2591.PNG"]
tags = []
title = "about"
upload = ""

+++
##### **_Born 2000. coded 2008. Univ. of Connecticut 2018. World Peace 2050._**

##### **✌️**

##### **🇭🇷/🇺🇸**

##### **_I 😍 Jean-Michel Basquiat, Giorgio Moroder, Travis Scott, Thundercat, Miles Davis, RZA & my mom._**

![](/uploads/RNI-Films-IMG-E0C34F47-200D-4E2F-B368-F7EA664ECB30.JPG)

**:rss:**

[**_:)_**](https://andy.haus/index.xml)

**:services this site is built upon:**

[**_monopriv_**](https://gitlab.com/kskarthik/monopriv) **|** [**_forestry_**](https://forestry.io/) **|** [**_netlify_**](https://www.netlify.com/)

**:git repositories for my code:**

[**_andy_**](https://gitlab.com/Andrew-Werner/monoblog) **|** [**_drew_**](https://github.com/Andrew-Werner/portfolioBuild)

**:thank you:**

[**_audiosynth_**](https://github.com/keithwhor/audiosynth "audiosynth")

[**_feedgrabbr_**](https://www.feedgrabbr.com/)

[**_this_**](https://pudding.cool/process/how-to-make-dope-shit-part-1/) **n** [**_this_**](https://pudding.cool/process/how-to-make-dope-shit-part-2/)